package com.devcamp.j60jparelationship;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J60jpaRelationShipApplication {

	public static void main(String[] args) {
		SpringApplication.run(J60jpaRelationShipApplication.class, args);
	}

}
