package com.devcamp.j60jparelationship.service;

import java.util.ArrayList;
import java.util.Set;

import com.devcamp.j60jparelationship.model.CCountry;
import com.devcamp.j60jparelationship.model.CRegion;
import com.devcamp.j60jparelationship.repository.ICountryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CountryService {
	@Autowired
	ICountryRepository pCountryRepository;
    
    public ArrayList<CCountry> getAllCountries() {
        ArrayList<CCountry> listCountry = new ArrayList<>();
        pCountryRepository.findAll().forEach(listCountry::add);
        return listCountry;
    }
    public Set<CRegion> getRegionsByCountryCode(String countryCode) { 
        CCountry vCountry = pCountryRepository.findByCountryCode(countryCode);
        if (vCountry != null) {
            return  vCountry.getRegions();
        } else {
            return null;
        }
    }
}
